package com.example.bootsample.web.form;

import javax.validation.constraints.NotEmpty;

public class TodoForm {

    private long id;

    @NotEmpty
    private String text;
    private boolean done;

    public TodoForm() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
