package com.example.bootsample.service;

import com.example.bootsample.entity.Todo;

import java.util.List;

public interface TodoService {
    List<Todo> findAll();

    Todo findById(long id);

    void add(String newTodoText);

    void done(long id);

    void remove(long id);
}
